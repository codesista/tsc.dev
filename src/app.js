    // modules
var express         = require('express');
var app             = express();
var morgan          = require('morgan');
var bodyParser      = require('body-parser');
var methodOverride  = require('method-override');
var passport        = require('passport');
var session         = require('express-session');
var cookies         = require('cookie-parser');
var sequelize       = require('sequelize');
var MySQLStore      = require('express-mysql-session')(session);
var models          = require('./models');
var options         = require('./config');
var auth            = require('./server/auth');
var path            = require('path');

app.disable('x-powered-by');
options.init(app);
var db = app.get('db');
var sitePath = app.get('sitePath');
const Sequelize = new sequelize(db.url, {
    dialect: db.dialect,
    logging: console.log
});
app.set('sequelize', Sequelize);
app.use(cookies());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.static(sitePath+'/public'));
var sessionStore = new MySQLStore(db);

app.use(morgan('combined'));
app.use(session({  
  store: sessionStore,
  secret: 'fancypantsmandylance',
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session()); 

app.use(bodyParser.json());

app.use(bodyParser.json({ type: 'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));

//require('./routes')(app);
require('./server/user').init(app);
require('./server/services').init(app);
require('./server/ticket').init(app);
require('./server/dashboard').init(app);
require('./server/payment').init(app);
require('./server/pages').init(app);

var listRouter = function()
{
    app._router.stack.forEach(function(r){
        if (r.route && r.route.path){
            console.log(r.route.path);
        }
    });
};
module.exports = app;
