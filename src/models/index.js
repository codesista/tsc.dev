const user = require('../server/user');
// const model2 = require('./model2');
const Sequelize = require('sequelize');

module.exports = function() {
  const app = this;
  const db = app.get('db');

  const sequelize = new Sequelize(db.url, {
    dialect: 'mysql',
    logging: console.log
  });

  app.set('sequelize', sequelize);

  app.configure(user);
  // app.configure(model1);
  // app.configure(model2);

  app.set('models', sequelize.models);

  Object.keys(sequelize.models).forEach(function(modelName) {
    if ('associate' in sequelize.models[modelName]) {
      sequelize.models[modelName].associate();
    }
  });

  sequelize.sync();
};