var passport    = require('passport');
var models      = require('../models/index');
var path        = require('path');
var fs          = require('fs');
var urls;
var sitePath;


function Pages (app) {
    urls = app.get('urls');
    sitePath = app.get('sitePath');
    var User = models.user;
    app.get('/:page', renderPage);
}

function renderPage(req,res)
{
    var blogPath = sitePath+'/public/blog/'+req.params.page+'/index.ejs';
    var pagePath = sitePath+'/public/pages/'+req.params.page+'.ejs';
    var ok = false;
    try {
        fs.accessSync(blogPath, fs.F_OK);
        res.render(sitePath+'/public/blog/'+req.params.page+'/index',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            page:       req.params.page,
            single:     true
        });
    } catch (e) {
        try {
            fs.accessSync(pagePath, fs.F_OK);
            res.render(sitePath+'/public/pages/'+req.params.page+'.ejs',{
                baseUrl:    urls.base,
                assets:     urls.assets,
                page:       req.params.page,
                single:     true
            });
        }
        catch(e)
        {
            res.render(sitePath+'/public/pages/404',{
                baseUrl:    urls.base,
                assets:     urls.assets,
                page:       '404',
                single:     true
            });
        }
    }
}

module.exports = Pages;