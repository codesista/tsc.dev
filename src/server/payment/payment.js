var randomstring    = require("randomstring")
var passport        = require('passport')
var mailer          = require('../mailer/mailer')
var models          = require('../models/index')
var stripe          = require("stripe")
var braintree       = require('braintree')
var path            = require('path')
var uuid            = require('node-uuid')
var fs              = require('fs')
var User            = models.user
var Plan            = models.plan
var Payment         = models.payment
var tscServices     = require('../services')
var urls
var sitePath
var gateway

function PaymentService (app) {
    gateway = braintree.connect({ accessToken: app.get('braintree') })
    urls = app.get('urls')
    sitePath = app.get('sitePath')
    stripe = stripe(app.get('stripe').secret)
    var User = models.user
    // app.post('/sckout', stripeCheckout)
    app.post('/sckout-xhr', stripeCheckoutXHR)
    app.get('/error', stripeCheckoutError)
    app.get('/pay/complete',completePayment)
    app.get('/pay/error',errorPayment)
    app.post('/pay/:id', renderPayment)
    app.get('/pay/:id', renderPayment)
    app.post("/btckout", processBraintreePayment);
}

function errorPayment(req,res) {
    res.render(sitePath+'/public/payment/err.ejs', {
        baseUrl:    urls.base,
        assets:     urls.assets,
        error:      true
    })
}
function completePayment(req,res) {
    Payment.findOne({
        where: {
            $and: [{id: req.query.p}]
        }
    })
    .then(function(payment){
        res.render(sitePath+'/public/payment/complete.ejs', {
            baseUrl:    urls.base,
            assets:     urls.assets,
            payment:    payment,
        })
    })
    .catch(function(error){
        console.log(error)
        res.render(sitePath+'/public/payment/err.ejs', {
            baseUrl:    urls.base,
            assets:     urls.assets,
            error:      error,
        })
    })
}

function processBraintreePayment(req,res)
{
    var pkg = tscServices.getPackage(req.body.srv,req.body.pkg)
    var orderId = new Date().toString()
    var info = {
        email:  req.body.email,
        first:  req.body.first,
        last:   req.body.last,  
        phone:  req.body.phone,
        issue:  req.body.issue
    }
    var saleRequest = {
        amount: req.body.amount,
        merchantAccountId: "USD",
        paymentMethodNonce: req.body.token.nonce,
        orderId: orderId,
        descriptor: {
            name: "tsc*"+pkg.name
        },
      options: {
        paypal: {
          customField: "TSC Order #"+orderId,
          description: pkg.service.name
        },
        submitForSettlement: true
      }
    }
    var trans;
    gateway.transaction.sale(saleRequest, function (err, result) {
        if (err) {
            res.status(500).json({ error: err })
        }
        else if (result.success) {
            var newPayment = {
                order_id: orderId,
                ip_address: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                email_address: info.email,
                first_name: info.first,
                last_name: info.last,
                phone_number: info.phone,
                issue_details: info.issue,
                transaction_id: result.transaction.id,
                transaction_details:JSON.stringify(result.transaction)   
            }
            trans = result.transaction
            Payment.create( newPayment )
            .then(function(payment) {
                res.cookie('transaction',JSON.stringify(trans), { maxAge: 900000, httpOnly: true })
                res.json(payment.id)
            })
            .catch(function(error){
                res.json(error)
            })
        }
        else {
            console.log(err)
            res.status(500).json({ error: 'not processed' })
            res.redirect('pay/error/');
        }
    })
}

function renderPayment(req,res)
{
    var tkn = false
    var pkgid = req.params.id.split('.')
    var pkg = tscServices.getPackage(pkgid[0],pkgid[1])
    var data = req.body

    if ( data.step == '2' )
    {
        gateway.clientToken.generate({}, function (err, response) {
            tkn = response.clientToken
            res.render(sitePath+'/public/pages/pay.ejs', {
                baseUrl:    urls.base,
                assets:     urls.assets,
                tkn:        tkn,
                pkg:        pkg,
                data:       data,
                single:     true
            })
        })
    }
    else
    {
        res.render(sitePath+'/public/pages/pay.ejs', {
            baseUrl:    urls.base,
            assets:     urls.assets,
            pkg:        pkg,
            data:       data,
            single:     true
        })
    }
}

function stripeCheckoutError(req,res)
{
    res.render('payment/error',{
        baseUrl:    urls.base,
        assets:     urls.assets,
        error:      "Payment processing error", 
    })    
}

function stripeCheckoutXHR(req,res)
{
    var customer =
    {
        email:          req.body.email,
        source:         req.body.id 
    }
    if ( req.body.planId !== 'single')
    {
        customer.plan = req.body.planId
    }
    console.log(customer)
    stripe.customers.create(customer)
    .then(function (customer){
        var description = req.body.planId !== 'single' ?
            " subscription charge for "+req.body.planId :
            " single ticket charge"
        return stripe.charges.create({
            amount:         req.body.price.replace(".", ""),
            currency:       "usd",
            description:    req.body.email+description,
            customer:       customer.id
        })
    })
    .then(function(charge){
        console.log("Stripe process successful")
        Plan.findOne({
            where: {
                $and: [{id: req.body.tscId}]
            }
        })
        .then(function(plan){
            var emailString = 'HASHED'
            var opts =
            {
                firstName   : req.body.onboard.firstName,
                lastName    : req.body.onboard.lastName,
                fullName    : req.body.onboard.firstName+' '+req.body.onboard.lastName,
                emailString : emailString,
                toEmail     : req.body.email,
                planName    : plan.niceName,
                planDetails : plan.description,
                fromEmail   : 'support@thesiteclinic.com',
                subject     : 'Welcome to The Site Clinic'
            }
            options = mailer.sendConfirmation(opts)
            console.log('Charged, and confirmation sent successfully')
            console.log(options)
            res.status(200).json(charge)
        })
        .catch(function(error)
        {
            console.log('Charged, but no confirmation sent')
            console.log(error)
            charge.sendGridError = error
            res.status(200).json(charge)
        })
    },
    function(err) {
        console.log(err)
        res.status(err.raw.statusCode).json(err.raw.message)
    })
}

module.exports = PaymentService