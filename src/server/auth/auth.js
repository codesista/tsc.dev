var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var authenticationMiddleware = require('./middleware');
var model = require('../models');

var User = model.user;

function errHandler(err) {
  console.error('There was an error performing the operation');
  console.log(err);
  console.log(err.code);
  return console.error(err.message);
}

passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: true
  },
  function(email, password, done) {
    User.findOne({
      where: {
        'email': email
      }
    })
    .then(function (user) {
        if (user === null) {
            console.log('User not found.');
            return done(null, false, { message: 'User not found.' })
        }
      //var hashedPassword = bcrypt.hashSync(password, user.salt)
        if (user.password == password) {
            return done(null, user)
        }
        
        return done(null, false, { message: 'Incorrect credentials.' })
    })
    .catch(function(error){
        return done(null, false, { message: 'User not found.' });
    })
  }
));

passport.serializeUser(function(user, done) {  
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {  
    User.findById(id).then(function(user) {
        done(null, user);
    }).catch(function(err) {
        done(err, null);
    });
});
passport.authenticationMiddleware = authenticationMiddleware;
module.exports = passport;