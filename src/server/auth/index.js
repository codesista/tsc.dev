module.exports = {
  init: require('./auth'),
  middleware: require('./middleware')
};