var passport    = require('passport');
var tscUtils    = require('../utils/tsc');
var models      = require('../models/index');
var path        = require('path');
var urls;
var User        = models.user;
var Ticket      = models.ticket;


function TicketService (app) {
    urls = app.get('urls');
    app.post('/tickets', passport.authenticationMiddleware(), createTicket);
    app.get('/dashboard/tickets', passport.authenticationMiddleware(), renderViewTickets);
    app.get('/dashboard/tickets/view/:id', passport.authenticationMiddleware(), renderSingleTicket);
    app.get('/dashboard/tickets/:action', passport.authenticationMiddleware(), renderCreateTicket);
}

function renderSingleTicket(req, res)
{
    Ticket.findOne({
        where: {
            $and: [{id: req.params.id}]
        }
    })
    .then(function(ticket){
        res.render('views/dashboard/tickets',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            user:       req.user,
            ticket:     ticket,
            screen:     "tickets",
            action:     "single"
        });
    })
    .catch(function(error){
        res.render('views/dashboard/tickets',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            user:       req.user,
            tickets:    false,
            screen:     "tickets",
            action:     "single.error"
        });
    })
}

function renderCreateTicket (req, res) {
    res.render('views/dashboard/tickets',{
        baseUrl:    urls.base,
        assets:     urls.assets,
        user:       req.user,
        tickets:    false,
        screen:     "tickets", 
        action:     req.params.action, 
    });
}

function renderViewTickets (req, res) {
    User.findOne({ where: { 'id': req.user.id }, include: [{ model: Ticket }]})
    .then(function(user){
        res.render('views/dashboard/tickets',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            user:       req.user,
            tickets:    user.tickets,
            screen:     "tickets",
            action:     "view"
        });
    })
    .catch(function(error){
        res.render('views/dashboard/tickets',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            user:       req.user,
            tickets:    false,
            screen:     "tickets",
            action:     "view"
        });
    })
}
function createTicket(req,res,next) {
    if ( !req.body.subject || !req.body.description || !req.body.careLevel )
        res.redirect('dashboard/tickets/create?err=1');

    if ( req.body.subject && req.body.description && req.body.careLevel )
    {
        var
        description = tscUtils.sanitizeText(req.body.description),
        subject     = tscUtils.sanitizeText(req.body.subject),
        user        = parseInt(req.user.id),
        care        = parseInt(req.body.careLevel);
        var newTicket =
        {
            ip_address:     req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            subject:        subject,
            description:    description,
            user_id:        user,
            care_level:     care,
            user: [
                { id: user }
            ]
        }

        Ticket.create( newTicket )
        .then(function(ticket) {
            console.log('ADDING USER #'+user)
            ticket.addUser(req.user.id);
            res.redirect('/dashboard/tickets/view/'+ticket.id);
        })
        .catch(function(error){
            res.render('views/dashboard/tickets',{
                baseUrl:    urls.base,
                assets:     urls.assets,
                user:       req.user,
                tickets:    false,
                screen:     "tickets",
                action:     "single.error",
                error:      error
            });
        })

    }
}

module.exports = TicketService;
