var tscUtils    = require('../utils/tsc')
var passport    = require('passport')
var models      = require('../models/index')
var path        = require('path')
var urls
var tscServices = require('../services/index')
var User = models.user
var Message = models.message


function initUser (app) {
    urls = app.get('urls')
    app.get('/', renderWelcome)
    app.get('/start', renderStart)
    app.get('/start/*', renderStart)
    app.get('/complete', renderWelcome)
    app.get('/login', renderLogin)
    app.get('/login/:err', renderLogin)
    app.post('/sckout-user', createUser)
    app.post('/contact', processContact)
    app.post('/login', passport.authenticate('local-login', { successRedirect: '/dashboard',
            failureRedirect: '/login?err=1' }))
    app.get('/logout', function(req, res){
        req.logout()
        res.redirect('/')
    })
    app.post('/signup', function(req, res){
        if (
            !req.body.username || !req.body.password || 
            !req.body.firstName || !req.body.lastName ||
            !req.body.email )
        {
            res.status(400).json({ error: 'Please pass name and password.' })
        }
        else {
            User.findAll({
                where: {
                    $or: [{username: req.body.username},{email: req.body.email}]
                }
            }).then(function(user){
                if (!user)
                {
                    User.create({
                        username: req.body.username,
                        password: req.body.password,
                        first_name: req.body.firstName,
                        last_name: req.body.lastName,
                        email: req.body.email
                    })
                    res.status(200).json({ success: 'user created' })
                }
                else
                { res.status(400).json({ error: 'user already exists' }) }
            })
        }
    })
}

function renderError(res,req,message)
{
    tscmessage =
    {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        emailAddy: req.body.emailAddy,
        message: req.body.message,
        error: message, 
    }
    res.render('pages/contact',{
        baseUrl:    urls.base,
        assets:     urls.assets,
        single:     true,
        action:     'default',
        tscmessage:      tscmessage
    })
}

function processContact(req,res)
{
    console.log('Processing a contact form submission')
    var tscmessage = {}
    if ( !req.body.firstName )
    {
        renderError(res,req,'Please provide your first name.')
        return
    }
    if ( !req.body.lastName )
    {
        renderError(res,req,'Please provide your last name.')
        return
    }
    if ( !req.body.emailAddy )
    {
        renderError(res,req,'Please provide a valid email address.')
        return
    }
    if ( !req.body.message )
    {
        renderError(res,req,'Please fill out the message part.')
        return
    }
    tscmessage.firstName = tscUtils.sanitizeText(req.body.firstName)
    tscmessage.lastName = tscUtils.sanitizeText(req.body.lastName)
    tscmessage.emailAddy = tscUtils.sanitizeEmail(req.body.emailAddy)
    tscmessage.message = tscUtils.sanitizeText(req.body.message)
    console.log(tscmessage)
    Message.create({
        ip_address: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        first_name: tscmessage.firstName,
        last_name:  tscmessage.lastName,
        email:      tscmessage.emailAddy,
        message:    tscmessage.message,
    })
    .then(function(user){
        res.render('pages/contact',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            single:     true,
            action:     'success'
        })
    })
    .catch(function(error){
        console.log(error)
        res.render('pages/contact',{
            baseUrl:    urls.base,
            assets:     urls.assets,
            single:     true,
            action:     'error',
            error:      error, 
        })
    })
}

function renderWelcome (req, res) {
    res.render('index',{
        baseUrl:    urls.base,
        assets:     urls.assets
    })
}
function renderStart (req, res) {
    console.log('Cookies: ', req.cookies)
    res.render('pages/services',{
        baseUrl:    urls.base,
        assets:     urls.assets
    })
}

function createUser(req,res) {
    if (
        !req.body.username || !req.body.firstName ||
        !req.body.lastName || !req.body.email )
    {
        res.status(400).json({ error: 'Malformed user' })
    }
    else
    {
        User.findOne({ where: {email: req.body.email} })
        .then(function(user){
            if (!user)
            {
                User.create({
                    // plan:       req.body.plan,
                    // ipAddress:  req.body.ipAddress,
                    username:   req.body.username,
                    password:   "*((((",
                    first_name:  req.body.firstName,
                    last_name:   req.body.lastName,
                    email:      req.body.email
                })
                .then(function(user){
                    res.status(200).json({ success: 'user created' })
                })
                .catch(function(error){
                    res.status(400).json({ error: error })
                })
            }
            else
            {
                error =
                {
                    code: 1
                }
                res.status(400).json({ error: error })
            }
        })
    }
}
function renderFailed(req,res)
{
    res.render('views/login',{
        baseUrl:    urls.base,
        assets:     urls.assets,
        error:      "Invalid username", 
    })
}

function renderLogin(req, res)
{
    var error = req.query.err == 1 ? true : false
    res.render('views/login',{
        baseUrl:    urls.base,
        assets:     urls.assets,
        single:     true,
        error:      error
    })
}

module.exports = initUser
