var passport        = require('passport');
var cookies         = require('cookie-parser');
var models          = require('../models/index');
var tscUtils        = require('../utils/tsc');
var User            = models.user;
var Ticket          = models.ticket;
var setup;
var urls;

function initDashboard (app) {
    urls = app.get('urls');
    app.get('/setup', renderAccountSetup);
    app.get('/setup/:err', renderAccountSetup);
    app.get('/dashboard', passport.authenticationMiddleware(), renderDashboardHome);
    app.get('/dashboard/setup', passport.authenticationMiddleware(), renderAccountSetup);
    app.get('/dashboard/:screen', passport.authenticationMiddleware(),renderDashboardHome);
    app.post('/dash-setup', setupAccount);
    app.get('/setupdash', renderAccountSetup);
}

function setupLogin(req, res, next)
{
  passport.authenticate('local-login', function(err, user, info) {
    if (err) { return next(err); }
    if (!setup) { return res.redirect('/login'); }
    req.logIn(setup, function(err) {
      if (err) { return next(err); }
      return res.redirect('/dashboard?first=1');
    });
  })(req, res, next);
}
function setupAccount(req,res,next)
{
    var User = models.user;
    var pass = req.body.password
    var confirm = req.body.passwordConfirm
    
    if ( !req.body.firstName || !req.body.lastName ||
        !req.body.email )
    {
        res.redirect('/login');
    }
    setup =
    {
        username:   req.body.email,
        email:      req.body.email,
        firstName:  req.body.firstName,
        lastName:   req.body.lastName,
        password:   req.body.password
    }
    if( !pass )
    {
        res.redirect('/setup?err=1')
    }
    else if ( pass !== confirm || !confirm )
    {
        res.redirect('/setup?err=2')
    }
    else
    {
        // console.log('Setting up user...');
        // console.log('Retrieving user from db');
        User.findOne({
            where: {
                $or: [{username: setup.username},{email: setup.email}]
            }
        }).then(function(user){
            // console.log('Hashing password');
            // console.log('Storing password');
            if (!user)
            {
                User.create({
                    username: setup.username,
                    password: setup.password,
                    first_name: setup.firstName,
                    last_name: setup.lastName,
                    email: setup.email
                })
                .then(function(user){
                    res.status(200).json({ success: 'user created' });
                });
            }
            user.update({
                password: setup.password
            })
            .then(function(user){
                setup.id = user.id
                setupLogin(req,res,next);
            })
            .catch(function (error) {
                res.status(400).json({ error: 'user could not be updated' });
            })
        });
    }
}
function renderAccountSetup (req, res) {
    var error = false;
    if (req.query.err === '1')
    {
        error = 'Please enter a password.'
    }
    if (req.query.err === '2')
    {
        error = 'Please make sure the passwords are the same.'
    }
    res.render('views/dashboard/setup/index', {       
        baseUrl:    urls.base,
        assets:     urls.assets,
        single:     true,
        error:      error
    });
}
function renderDashboardHome (req, res) {
    if ( req.query.first == 1 )
    {
        var onboard = req.cookies;
        var description = tscUtils.sanitizeText(onboard.summary);
        var newTicket =
        {
            ip_address:     req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            subject:        "My first ticket.",
            description:    description,
            user_id:        req.user.id,
            care_level:     onboard.critical,
            user: [
                { id: req.user.id }
            ]
        }
        Ticket.create( newTicket )
        .then(function(ticket) {
            ticket.addUser(req.user.id);
            User.findOne({ where: { 'id': req.user.id }, include: [{ model: Ticket }]})
            .then(function(user){
                res.clearCookie('firstName');
                res.clearCookie('lastName');
                res.clearCookie('em');
                res.clearCookie('onboard');
                res.clearCookie('summary');
                res.clearCookie('critical');
                res.clearCookie('tscPlan');
                var screen = 'index';
                var type = 'user';
                if (user.role_id != 0 )
                {
                    type = 'technician'
                }
                if (req.params.screen)
                {
                    screen = req.params.screen;
                }
                res.render('views/dashboard/'+screen,{
                    baseUrl:    urls.base,
                    assets:     urls.assets,
                    screen:     screen,
                    tickets:    user.tickets,
                    user:       req.user,
                    type:       type
                });
            })
        })
        .catch(function(error){
            var screen = 'index';
            var type = 'user';
            if (user.role_id != 0 )
            {
                type = 'technician'
            }
            if (req.params.screen)
            {
                screen = req.params.screen;
            }
            res.render('views/dashboard/'+screen,{
                baseUrl:    urls.base,
                assets:     urls.assets,
                screen:     screen,
                tickets:    user.tickets,
                user:       req.user,
                type:       type
            });
        })
    }
    else
    {
        User.findOne({ where: { 'id': req.user.id }, include: [{ model: Ticket }]})
        .then(function(user){
            var screen = 'index';
            var type = 'user';
            if (user.role_id != 0 )
            {
                type = 'technician'
            }
            if (req.params.screen)
            {
                screen = req.params.screen;
            }
            res.render('views/dashboard/'+screen,{
                baseUrl:    urls.base,
                assets:     urls.assets,
                screen:     screen,
                tickets:    user.tickets,
                user:       req.user,
                type:       type
            });
        })
    }
}

module.exports = initDashboard;
