module.exports = {
  init: require('./mailer'),
  sendConfirmationEmail: require('./sendConfirmationEmail')
};