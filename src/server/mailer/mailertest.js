var passport        = require('passport');
var mailer          = require('../mailer/index');
var helper          = require('sendgrid').mail
var sg              = require('sendgrid')("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");
var models          = require('../models/index');
var urls;


function MailerTest (app) {
    urls = app.get('urls');
    app.post('/email', sendGridTest);
    app.get('/emailtest',sendGridGot);
}

function sendGridTest(req,res)
{
    req.body.first = "Ann";
    req.body.fullname = "Ann Daramola";
    var planName = "Bronze Monthly Plan";
    var planDetails = "File up to 5 tickets each month, and we'll turn them around within the week. Cancel or upgrade anytime.";
    var emailString = "adlfakioafaasalk"
    var fromEmail = new helper.Email('support@thesiteclinic.com');
    var toEmail = new helper.Email(req.body.email);
    var subject = 'Welcome to The Site Clinic';
    var content = new helper.Content(
      'text/html', 'We\'re glad to be able to serve you.');
    var mail = new helper.Mail(fromEmail, subject, toEmail, content);
    
    mail.personalizations[0].addSubstitution(
      new helper.Substitution('-TSCFullName-', req.body.fullname));
    mail.personalizations[0].addSubstitution(
      new helper.Substitution('-TSCPlanName-', planName));
    mail.personalizations[0].addSubstitution(
      new helper.Substitution('-TSCPlanDetails-', planDetails));
    mail.personalizations[0].addSubstitution(
      new helper.Substitution('-TSCEMAILSTRING-', emailString));

    mail.setTemplateId('9226a1bc-c350-4227-b450-8caa73fa8ac1');

    var request = sg.emptyRequest({
      method: 'POST',
      path: '/v3/mail/send',
      body: mail.toJSON(),
    });

    sg.API(request, function(error, response) {
      console.log(response.statusCode);
      console.log(response.body);
      console.log(response.headers);
    }); 

    res.redirect('/emailtest');
}

function sendGridGot(req,res) {
    res.status(200);
    res.render('views/email-test',{
        baseUrl:    urls.base,
        assets:     urls.assets,
        single:     true
    });
}

module.exports = MailerTest;