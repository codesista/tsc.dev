var passport        = require('passport');
var helper          = require('sendgrid').mail;
var sg              = require('sendgrid')("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");
var models          = require('../models/index');
var urls;

module.exports = {
    sendConfirmation: function(opts)
    {
        // make sure I got my opts in a row
        var options = {}
        options.firstName = opts.firstName ? opts.firstName : "First";
        options.lastName = opts.lastName ? opts.lastName: "Last";
        options.fullName = opts.fullName ? opts.fullName : options.firstName+' '+options.lastName;

        options.planName = opts.planName
        options.planDetails = opts.planDetails ? opts.planDetails : false
        options.emailString = opts.emailString ? opts.emailString : false
        options.fromEmail = opts.fromEmail ? opts.fromEmail : false
        options.toEmail = opts.toEmail ? opts.toEmail : false

        options.subject = opts.subject ? opts.subject : "Welcome to The Site Clinic";
        options.templateId = opts.templateId ? opts.templateId : '9226a1bc-c350-4227-b450-8caa73fa8ac1';

        var fromEmail = new helper.Email(options.fromEmail);
        var toEmail = new helper.Email(opts.toEmail);
        var content = new helper.Content(
            'text/html',
            'Thank you for letting us serve you.'
        );

        var mail = new helper.Mail(fromEmail, options.subject, toEmail, content);
        
        mail.personalizations[0].addSubstitution(
          new helper.Substitution('-TSCFullName-', options.fullName));
        mail.personalizations[0].addSubstitution(
          new helper.Substitution('-TSCPlanName-', options.planName));
        mail.personalizations[0].addSubstitution(
          new helper.Substitution('-TSCPlanDetails-', options.planDetails));
        mail.personalizations[0].addSubstitution(
          new helper.Substitution('-TSCEMAILSTRING-', options.emailString));

        mail.setTemplateId(options.templateId);

        var request = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: mail.toJSON(),
        });

        options.errors = false;

        sg.API(request, function(error, response) {
          options.errors = response;
        }); 

        return options;
    }
};
