var passport    = require('passport')
var models      = require('../models/index')
var path        = require('path')
var fs          = require('fs')
var urls
var sitePath
var tscServiceData = require('./data')

function getServiceById(id)
{
    for (var i = 0; i < tscServiceData.length; i++) {
        if ( tscServiceData[i].id === parseInt(id) )
            return tscServiceData[i]
    }
    return false;
}
function getServiceBySlug(slug)
{
    for (var i = 0; i < tscServiceData.length; i++) {
        if ( tscServiceData[i].slug == slug )
            return tscServiceData[i]
    }
    return false;
}

function tscServices (app) {
    urls = app.get('urls')
    sitePath = app.get('sitePath')
    app.get('/services/', renderServices)
    app.get('/services/:slug', renderPage)
}

function renderServices(req,res) {
    res.render(sitePath+'/public/pages/services.ejs', {
        baseUrl:    urls.base,
        assets:     urls.assets,
        services:    tscServiceData
    })
}

function renderPage(req,res)
{
    var pagePath = sitePath+'/public/pages/'+req.params.page+'.ejs'
    var ok = false
    var data = tscServiceData[req.params.id]
    var data = getServiceBySlug(req.params.slug)
    res.render(sitePath+'/public/pages/single-service.ejs', {
        baseUrl:    urls.base,
        assets:     urls.assets,
        service:    data,
        single:     true
    })
}

module.exports = tscServices