module.exports = [
    {
        id: 1,
        name: 'Websites & WordPress',
        slug: 'websites-and-wordpress',
        description: 'This is a wordpress package',
        icon: 'pe-7s-browser', 
        packages: [
            {
                id: 1,
                name: 'Quick Fix',
                icon: 'pe-7s-tools',
                description: '',
                features: [
                    'one plugin fix or installation or',
                    'one theme fix or installation or',
                    'HTML/CSS fixes for one web page or',
                    'one domain name service (transfering, registering, or renewing one domain)'
                ],
                cost: 59,
                frequency: 'issue'
            },
            {
                id: 2,
                name: 'Emergency Repair',
                icon: 'pe-7s-gleam',
                description: '',
                features: [
                    'repair a broken or hacked website within 48 hours or',
                    'completion of up to 2 hours of work left by your web developer or',
                    'troubleshoot and repair critical issues'
                ],
                cost: 199
            },
            {
                id: 3,
                name: 'Maintenance',
                icon: 'pe-7s-config',
                description: '',
                features: [
                    'up to 3 hours of website maintenance each month',
                    'support for wordpress, drupal, squarespace, ghost, and other content management systems',
                    '30-day rollover for unused hours',
                    'monthly offsite backups',
                    'monthly security reports',
                    '24 hour turn around for most tickets'
                ],
                cost: 89,
                frequency: 'month'
            }
        ]
    },
    {
        id: 2,
        name: 'Email Marketing',
        slug: 'email-marketing',
        description: 'This is the marketing',
        icon: 'pe-7s-volume', 
        packages: [
            {
                id: 4,
                name: 'Email Makeover',
                icon: 'pe-7s-pen',
                description: '',
                features: [
                    'redesign service for up to 3 email templates',
                    'responsive design for mobile device visibility',
                    'compatible with most major email service providers'
                ],
                cost: 99
            },
            {
                id: 5,
                name: 'Email Jumpstart',
                icon: 'pe-7s-next',
                description: '',
                features: [
                    'account set up for most major email service providers',
                    'one email newsletter template design',
                    'mailing list import and configuration'
                ],
                cost: 199
            },
            {
                id: 6,
                name: 'Email Automation',
                icon: 'pe-7s-graph1',
                description: '',
                features: [
                    'marketing funnel set up for up to 5 emails',
                    'responsive design on all emails for mobile device visibility',
                    'compatible with most major email service provicers'
                ],
                cost: 299
            }
        ]
    },
    {
        id: 3,
        name: 'Branding & Identity',
        slug: 'branding-and-identity',
        description: '',
        icon: 'pe-7s-user-female', 
        packages: [
            {
                id: 7,
                name: 'Personal Branding',
                icon: 'pe-7s-user-female',
                description: '',
                features: [
                    '30-minute strategy jumpstart session',
                    'one mood board / brand canvas',
                    'one domain set up',
                    'personalized email set up',
                    '3 social media account jumpstarts',
                    '1 website design'
                ],
                cost: 599
            },
            {
                id: 8,
                name: 'Logo Design',
                icon: 'pe-7s-id',
                description: '',
                features: [
                    '60-minute strategy jumpstart session',
                    'one mood board / brand canvas',
                    'up to 3 revision cycles',
                    'up to 3 print-ready logo files',
                    'up to 3 web-ready logo files'
                ],
                cost: 699
            },
            {
                id: 9,
                name: 'Product Promo',
                icon: 'pe-7s-photo',
                description: '',
                features: [
                    '60-minute strategy jumpstart session',
                    'one mood board / brand canvas',
                    'up to 3 revision cycles',
                    '1 product landing page',
                    'up to 20 digital images (custom memes, images, etc.)',
                    'fully branded web product (ebooks, web courses, or other digital products)'
                ],
                cost: 799
            }
        ]
    },
    {
        id: 4,
        name: 'Businesses & Institutions',
        slug: 'businesses-and-institutions',
        description: '',
        icon: 'pe-7s-monitor', 
        packages: [
            {
                id: 10,
                name: 'Monthly Maintenance',
                icon: 'pe-7s-config',
                description: '',
                features: [
                    'up to 10 hours of service each month',
                    'support for wordpress, squarespace, and other content management systems',
                    'monthly offsite backups and security reports',
                    '1 hour response time',
                    '24 hour turn around for most tickets'
                ],
                cost: 599,
                frequency: 'month'
            },
            {
                id: 11,
                name: 'Security and Recovery',
                icon: 'pe-7s-lock',
                description: '',
                features: [
                    'offsite secure backups of webpages and databases',
                    'database security review',
                    'website malware scan',
                    'data recovery and website restoration'
                ],
                cost: 249,
                frequency: 'month'
            },
            {
                id: 12,
                name: 'Website Audit',
                icon: 'pe-7s-display2',
                description: '',
                features: [
                    'website speed report',
                    'database optimization',
                    'webhost optimization',
                    'code audits'
                ],
                cost: 249
            }
        ]
    },
    {
        id: 5,
        name: 'Jumpstart Packages',
        slug: 'jumpstart-packages',
        icon: 'pe-7s-next', 
        description: 'This is a concierge package',
        packages: [
            {
                id: 13,
                name: 'WordPress Jumpstart',
                icon: 'pe-7s-next',
                description: 'Got a website that needs to be up ASAP? This package is for you.',
                features: [
                    '30-minute strategy / jumpstart session',
                    '1 theme installation',
                    'up to 3 plugin installations',
                    'up to 3 revision cycles',
                    '1 domain setup',
                    '1 webhost setup',
                    '48 hour turnaround'
                ],
                cost: 699
            },
            {
                id: 14,
                name: 'Personal Branding',
                icon: 'pe-7s-user-female',
                description: 'Ready to put yourself out there and start getting noticed for your expertise? This package is for you.',
                features: [
                    '30-minute strategy / jumpstart session',
                    '1 website setup on the platform of your choice',
                    '1 domain setup',
                    '1 personal branded email setup',
                    'up to 3 revision cycles',
                    '72 hour turnaround'
                ],
                cost: 499
            },
            {
                id: 15,
                name: 'Social Media',
                icon: 'pe-7s-display1',
                description: 'Ready to start engaging your community? This package is for you',
                features: [
                    '30-minute strategy / jumpstart session',
                    'up to 3 social media account setup or makeover',
                    'up to 20 branded memes and/or content marketing items',
                    '1 custom 3-month strategy guide',
                    '1 week turnaround'
                ],
                cost: 399
            },
            {
                id: 16,
                name: 'Email Marketing',
                icon: 'pe-7s-mail-open-file',
                description: 'Ready to start building your mailing list? This package is for you.',
                features: [
                    '30-minute strategy session',
                    '1 email service provider setup',
                    'up to 3 banded email templates',
                    '1 automated email funnel design (up to 5 emails)',
                    '1 lead generation page template'
                ],
                cost: 399
            }
        ]
    },
    {
        id: 6,
        name: 'Other Services',
        slug: 'other-services',
        icon: 'pe-7s-star', 
        description: 'Need something else?',
        packages: [
            {
                id: 17,
                name: 'Custom Solutions',
                icon: 'pe-7s-config',
                description: 'Didn\'t find what you need here? Please get in touch so we can create a custom package just for you.'
            }
        ]
    }
]