var allServices = require('./data')
module.exports = {
    init: require('./services'),
    data: allServices,
    getPackage: function (serviceId,packageId)
    {
        for (var i = 0; i < allServices.length; i++) {
            if ( allServices[i].id === parseInt(serviceId) )
            {
                var thisService = allServices[i]
                for (var j = 0; j < thisService.packages.length; j++) {
                    var thisPackage = thisService.packages[j]
                    if ( thisPackage.id === parseInt(packageId) )
                    {
                        thisPackage.service = thisService
                        return thisPackage;
                    }
                }
            }
        }
        return false;
    }
}
