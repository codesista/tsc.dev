'use strict';
module.exports = function(sequelize, DataTypes) {
  var plan = sequelize.define('plan', {
    name: DataTypes.STRING,
    niceName: DataTypes.STRING,
    description: DataTypes.TEXT,
    price: DataTypes.STRING,
    frequency: DataTypes.STRING,
    stripe_id: DataTypes.STRING,
    paypal_id: DataTypes.STRING,
    braintree_id: DataTypes.STRING,
    deleted_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        plan.belongsToMany(models.user, {
          through: 'user_plan',
          foreignKey: 'plan_id',
        });
        // plan.hasMany(models.feature)
      }
    },
    underscored: true
  });
  return plan;
};