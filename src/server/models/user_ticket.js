'use strict';
module.exports = function(sequelize, DataTypes) {
  var userTicket = sequelize.define('user_ticket', {
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ticket_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  },
  {
    tableName: 'user_ticket',
    timestamps: false,
    underscored: true
  }
  );
  return userTicket;
};