'use strict';
module.exports = function(sequelize, DataTypes) {
  var message = sequelize.define('message', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: DataTypes.STRING,
    message: DataTypes.TEXT,
    ip_address: DataTypes.STRING,
    deleted_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    underscored: true
  });
  return message;
};