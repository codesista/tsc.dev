'use strict';
module.exports = function(sequelize, DataTypes) {
  var payment = sequelize.define('payment', {
    order_id: DataTypes.STRING,
    ip_address: DataTypes.STRING,
    email_address: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    issue_details: DataTypes.TEXT,
    transaction_id: DataTypes.STRING,
    transaction_details: DataTypes.STRING,
    deleted_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    underscored: true
  });
  return payment;
};