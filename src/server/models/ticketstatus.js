'use strict';
module.exports = function(sequelize, DataTypes) {
  var ticketStatus = sequelize.define('ticketStatus', {
    name: DataTypes.STRING,
    deleted_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
      underscored: true
  });
  return ticketStatus;
};