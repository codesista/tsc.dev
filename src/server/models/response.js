'use strict';
module.exports = function(sequelize, DataTypes) {
  var response = sequelize.define('response', {
    message:  DataTypes.TEXT,
    userId:     { type:DataTypes.INTEGER },
    ticket_id:   { type:DataTypes.INTEGER },
    parent_id:   { type:DataTypes.INTEGER },
    viewed_on:   { type:DataTypes.INTEGER },
    deleted_at:  { type:DataTypes.DATE }
  }, {
    classMethods: {
      associate: function(models) {
        response.belongsTo(models.user, {
          onDelete: "CASCADE",
          foreignKey: {
            allowNull: false
          }
        })
        response.belongsTo(models.ticket, {
          onDelete: "CASCADE",
          foreignKey: {
            allowNull: false
          }
        })
      },
      underscored: true
    }
  });
  return response;
};