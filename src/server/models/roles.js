'use strict';
module.exports = function(sequelize, DataTypes) {
  var role = sequelize.define('role', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    deleted_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        role.hasMany(models.user,{foreignKey: 'id'})
      }
    },
    underscored: true
  });
  return role;
};