'use strict';
module.exports = function(sequelize, DataTypes) {
  var ticket = sequelize.define('ticket', {
    description: DataTypes.TEXT,
    subject: DataTypes.STRING(100),
    status: DataTypes.INTEGER,
    ip_address: DataTypes.STRING,
    user_id: { type:DataTypes.INTEGER },
    care_level: { type:DataTypes.INTEGER },
    deleted_at: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        ticket.belongsToMany(models.user, {
          through: 'user_ticket',
          foreignKey: 'ticket_id',
          onDelete: "CASCADE"
        })
      }
    },
    underscored: true
  });
  return ticket;
};