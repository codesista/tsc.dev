var passport    = require('passport');
var models      = require('../models/index');
var setup;
var urls;

function Technician (app) {
    urls = app.get('urls');
    app.get('/dashboard', passport.authenticationMiddleware(), renderDashboardHome);
    app.get('/dashboard/setup', passport.authenticationMiddleware(), renderAccountSetup);
    app.get('/dashboard/:screen', passport.authenticationMiddleware(),renderDashboardHome);
    app.post('/dash-setup', setupAccount);
    app.get('/setupdash', renderAccountSetup);
}

function setupLogin(req, res, next)
{
  passport.authenticate('local-login', function(err, user, info) {
    if (err) { return next(err); }
    if (!setup) { return res.redirect('/login'); }
    req.logIn(setup, function(err) {
      if (err) { return next(err); }
      return res.redirect('/dashboard');
    });
  })(req, res, next);
}
function setupAccount(req,res,next)
{
    var User = models.user;
    var pass = req.body.password
    var confirm = req.body.passwordConfirm
    console.log(!req.body.firstName || !req.body.lastName ||
        !req.body.email);
    if ( !req.body.firstName || !req.body.lastName ||
        !req.body.email )
    {
        res.redirect('/login');
    }
    setup =
    {
        username:   req.body.email,
        email:      req.body.email,
        firstName:  req.body.firstName,
        lastName:   req.body.lastName,
        password:   req.body.password
    }
    if( !pass )
    {
        res.redirect('/setup?err=1')
    }
    else if ( pass !== confirm || !confirm )
    {
        res.redirect('/setup?err=2')
    }
    else
    {
        console.log('Setting up user...');
        console.log('Retrieving user from db');
        User.findOne({
            where: {
                $or: [{username: setup.username},{email: setup.email}]
            }
        }).then(function(user){
            console.log('Hashing password');
            console.log('Storing password');
            if (!user)
            {
                User.create({
                    username: setup.username,
                    password: setup.password,
                    first_name: setup.firstName,
                    last_name: setup.lastName,
                    email: setup.email
                });
                res.status(200).json({ success: 'user created' });
            }
            user.update({
                password: setup.password
            })
            .then(function(user){
                setup.id = user.id
                setupLogin(req,res,next);
            })
            .catch(function (error) {
                res.status(400).json({ error: 'user could not be updated' });
            })
        });
    }
}
function renderAccountSetup (req, res) {
    var error = false;
    if (req.query.err === '1')
    {
        error = 'Please enter a password.'
    }
    if (req.query.err === '2')
    {
        error = 'Please make sure the passwords are the same.'
    }
    res.render('views/dashboard/setup/index', {       
        baseUrl:    urls.base,
        assets:     urls.assets,
        single:     true,
        error:      error
    });
}
function renderDashboardHome (req, res) {
    var screen = 'index';
    if (req.params.screen)
        screen = req.params.screen;
    res.render('views/dashboard/'+screen,{
        baseUrl:    urls.base,
        assets:     urls.assets,
        screen:     screen
    });
}

module.exports = Technician;
