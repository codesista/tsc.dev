var sanitize = require('sanitize-caja');

module.exports = {
    sanitizeText: function(text)
    {
        return sanitize(text)
    },
    sanitizeEmail: function (email) {
        return sanitize(email)
    }
};