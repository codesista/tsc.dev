function MainController($scope, $state) {
    $scope.$on('$stateChangeSuccess', function(newValue, oldValue) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if ( newValue !== oldValue ) {
            $scope.bodyClass = $state.current.viewClass;
            screenClass( $state.current.screen );
        }
        $("#home-cta").backstretch("/assets/img/tsc-women.jpg");
    });
    $scope.tagline = 'Live long and dougie.';
}
MainController.$inject = ['$scope', '$state'];