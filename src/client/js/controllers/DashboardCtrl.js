function DashboardController($scope, $state, $cookies) {
    $scope.$on('$stateChangeSuccess', function(newValue, oldValue) {
        $scope.screen = $state.current.screen;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });

    $scope.onboard = {
        firstName:      $cookies.get('firstName').replace(/\"/g, ""),
        lastName:       $cookies.get('lastName').replace(/\"/g, ""),
        email:          $cookies.get('em').replace(/\"/g, "")
    }

}
DashboardController.$inject = ['$scope', '$state','$cookies'];