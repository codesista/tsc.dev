var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
function StartController($state, $scope, $cookies, $cookieStore) {
    $scope.plans =
    {
        single:     { 
            name:       "single",
            niceName:   "Single Issue Only",
            price:      "59.99",
            sprice:      5999,
            frequency:  'hour',
            splan:      __env.stripePlanIds.single,
            features:   [
                "One issue at a time",
                "Max 5 day turn around",
                "1 domain"
            ],
            pp:         "J5U7WTJ7VU3MS",
            tscId:      1,
            email:      "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=J5U7WTJ7VU3MS"
        },
        bronze:     { 
            name:       "bronze",
            niceName:   "Bronze Monthly Plan",
            price:      "79.99",
            sprice:      7999,
            frequency:  'month',
            splan:      __env.stripePlanIds.bronze,
            features:   [
                "Up to 5 issues per month",
                "Max 5 day turn around",
                "1 domain"
            ],
            pp:         "J5U7WTJ7VU3MS",
            tscId:      2,
            email:      "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=J5U7WTJ7VU3MS"
        },
        silver:     { 
            name:       "silver",
            niceName:   "Silver Monthly Plan",
            price:      "99.99",
            sprice:      9999,
            frequency:  'month',
            splan:      __env.stripePlanIds.silver,
            features:   [
                "Up to 10 jobs per month",
                "Max 3 day turn around",
                "2 domains"
            ],
            pp:         "AX6SASMYCYPEE",
            tscId:      3,
            email:      "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=AX6SASMYCYPEE"
        },
        gold:     { 
            name:       "gold",
            niceName:   "Gold Monthly Plan",
            price:      "199.99",
            sprice:      19999,
            frequency:  'month',
            splan:      __env.stripePlanIds.gold,
            features:   [
                "Up to 15 jobs per month",
                "Max 2 day turn around",
                "3 domains"
            ],
            pp:         "MMJPUEHR97UYE",
            tscId:      4, 
            email:      "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=MMJPUEHR97UYE" 
        }
    };
    var one = $cookieStore.get('summary') ? true : false;
    var two = $cookieStore.get('critical') ? true : false;
    $scope.nextSteps =
    {
        one:    one,
        two:    two,
        three:  false,
        four:   false
    };
    $scope.here = false;
    $scope.processing = false;
    $scope.onboard =
    {
        em:         $cookieStore.get('em') ? $cookieStore.get('em') : '',
        firstName:  $cookieStore.get('firstName') ? $cookieStore.get('firstName') : '',
        lastName:   $cookieStore.get('lastName') ? $cookieStore.get('lastName') : '',
        plan:       $cookieStore.get('tscPlan') ? $scope.plans[$cookieStore.get('tscPlan')] : $scope.plans.silver,
        critical:   $cookieStore.get('critical') ? $cookieStore.get('critical') : 1,
        summary:    $cookieStore.get('summary') ? $cookieStore.get('summary') : ''
    }

    $scope.em = $cookieStore.get('em') ? $cookieStore.get('em') : '';
    $scope.plan = $cookieStore.get('tscPlan') ? $scope.plans[$cookieStore.get('tscPlan')] : $scope.plans.silver;
    $scope.screen = $state.current;
    $scope.$on('$stateChangeSuccess', function(newValue, oldValue) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if ( $state.current.name == "start.pending" && !$scope.processing )
        {
            $state.go('start.1');
        }
        if ( $state.current.name == "start" )
        {
            $state.go('start.1');
        }
        $scope.currentStep = $state.current.step
        if ( newValue !== oldValue ) {
            $scope.bodyClass = $state.current.screen;
            screenClass( $state.current.screen );
            $scope.start = $state.current.name === "start";
            $scope.email = $state.current.name == "start.email";
            $scope.pay = $state.current.name == "start.pay";
            $scope.steps = $state.current.stage == "steps";
            $scope.pending = $state.current.name == "start.pending"
            $scope.screen = $state.current;
            $scope.continue = false;
            if( $scope.email )
            {
                $scope.screen.title = $scope.plan.name + " care plan";
            }
            $(".page-start").backstretch("/assets/img/tsc-woman-phone.jpg");
            window.scrollTo(0, 0);
        }
    });

    $scope.greeting = "Welcome";
    $scope.screen = "onboard";
    $scope.q = [];
    $scope.curr = 1;
    $scope.showNext = function (step)
    {
        $('.signuperrors').fadeOut('fast');
        $scope.continue = $scope.onboardFormIsValid();
    };
    $scope.capturePlan = function (plan)
    {
        if ( $scope.plan.name !== plan || !$scope.here )
            $scope.toggleFeatures(plan);
        $scope.plan = $scope.plans[plan];
        $cookieStore.put('tscPlan',plan);
        $scope.continue = true;
        $scope.here = true;
    };
    $scope.isEm = function(em)
    {
        return EMAIL_REGEXP.test( em );
    }
    $scope.onboardFormIsValid = function()
    {
        if ( $('#em').val() && $scope.isEm( $('#em').val() )
            && $('#fname').val() && $('#lname').val() )
        {
            $cookieStore.put('em', $('#em').val());
            $cookieStore.put('firstName', $('#fname').val());
            $cookieStore.put('lastName', $('#lname').val());
            $scope.onboard.em = $('#em').val();
            $scope.onboard.firstName = $('#fname').val();
            $scope.onboard.lastName = $('#lname').val();
            return true;
        }
        return false;
    }
    $scope.toggleFeatures = function(plan)
    {
        $(".plan-features").fadeOut(200);
        $("#"+plan+" .plan-features").slideDown(400);
    }
    $scope.openStripe = function(handler)
    {
        handler.open({
            zipCode: true,
            name: $scope.plan.niceName,
            email: $scope.onboard.em,
            amount: $scope.plan.sprice
        });        
    }
    $scope.processStripe = function()
    {
        $state.go('start.pending');
        $scope.processing = true;
        var errors = false;
        var stripeKey = __env.stripeKey;
        var handler = StripeCheckout.configure({
            // key: 'pk_test_SnKHGG2vwSUrId9fptCfxHlN',
            key: stripeKey, 
            image: '',
            locale: 'auto',
            description: 'CARE PLAN',
            token: function(token) {
                token.planId    = $scope.plan.splan;
                token.tscId     = $scope.plan.tscId;
                token.price     = $scope.plan.sprice;
                token.onboard   = $scope.onboard
                var jqxhr = $.post( "/sckout-xhr", token, function(data) {
                })
                  .done(function() {
                    $cookies.put('onboard',$scope.onboard);
                    $state.go('complete');
                  })
                  .fail(function(data) {
                    $scope.error = {
                        msg: data.responseJSON
                    };
                    $state.go('start.error');
                  })
                  .always(function() {
                });
            }
        });
        $scope.openStripe(handler);

        window.addEventListener('popstate', function() {
          handler.close();
        });
    }
    $scope.nextStep = function(currentStep, value)
    {
        currentStep = parseInt(currentStep);
        if ( currentStep == 1 )
        {
            $scope.processStepOne();
        }
        if ( currentStep == 2 )
        {
            $cookieStore.put('critical',value);
            $scope.processStepTwo();
        }
    }
    $scope.processStepOne = function()
    {
        if ( $('#step1 textarea').val().length > 25 )
        {
            $scope.nextSteps.one = true;
        }
        else
        {
            $scope.nextSteps.one = false;
        }
    }
    $scope.processStepTwo = function()
    {
        $scope.nextSteps.two = true;
    }
    $scope.goTo = function(step)
    {
        if (step == 2 )
        {
            var summary = $("#summary").val()
            $cookieStore.put('summary',summary);
        }
        if (step == 4 )
        {
            var user = 
            {
                // plan:       $scope.plan.splan, 
                username:   $scope.onboard.em,
                email:      $scope.onboard.em,
                firstName:  $scope.onboard.firstName,
                lastName:  $scope.onboard.lastName
            }
            result = setupUser(user,$state)
        }
        else
        {
            $state.go('start.'+step);
        }
    }
    $scope.backStep = function(step)
    {
        step = parseInt(step);
        $state.go('start.'+step);
    }

}
function setupUser(user, state)
{
    var results = 
    {
    }
    var jqxhr = $.post( "/sckout-user", user, function(data) {
    })
    .done(function() {
        state.go('start.4')
    })
    .fail(function(data) {
        if ( data.responseJSON.error.code )
        {
            $('.signuperrors.error'+ data.responseJSON.error.code).fadeIn()
        }
        results.errors = true;
        results.errors.msg = data.error;
    })
    .always(function(data) {
        results.data = data;
    });
    return results;
}
StartController.$inject = ['$state','$scope','$cookies','$cookieStore'];