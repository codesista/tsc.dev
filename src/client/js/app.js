tsc = angular.module('tscApp',
    [
        "ui.router",
        "ngCookies",
        "appRoutes"
    ]);
tsc.controller('MainController', MainController);
tsc.controller('StartController', StartController);
tsc.controller('DashboardController', DashboardController);
//helpers
function screenClass(screen)
{
    $("body").removeClass (function (index, css) {
        return (css.match (/\bscreen-\S+/g) || []).join(' ');
    });
    $('body').addClass('screen-'+screen);
}


var waypoints = $('#header').waypoint(function(direction) {
  if (direction === 'down' )
    $('#header').addClass('scrolled');
  else
    $('#header').removeClass('scrolled');
}, {offset: -20});


