function setupStates(stateProvider,states)
{
    for (var i = 0, len = states.length; i < len; i++) {
        stateProvider.state(states[i]);
    }
    return stateProvider;  
}

angular.module('appRoutes',[])
    .constant('__env', __env)
    .config(['$locationProvider', '$stateProvider',
    function( $locationProvider, $stateProvider) {
        var onboarding = {
            name: 'start',
            url: '/start',
            controller: 'StartController',
            templateUrl: 'views/start.html',
            screen: 'onboarding',
            title: ""

        };
        var completeState = {
            name: 'complete',
            url: '/complete',
            controller: 'StartController',
            templateUrl: 'views/complete.html',
            screen: 'onboarding',
            title: "Payment Complete"
        };
        var onboardingScreens = [
            {
                name: 'start.email',
                url: '/email',
                templateUrl: 'views/onboard/forms/email.html',
                screen: 'onboarding'
            },
            {
                name: 'start.pay',
                url: '/pay',
                templateUrl: 'views/onboard/forms/pay.html',
                screen: 'onboarding',
                title: "Payment"
            },
            {
                name:           'start.pending',
                url:            '/pending',
                templateUrl:    'views/onboard/forms/pending.html',
                screen:         'onboarding',
                title:          'Processing Payment'
            },
            {
                name:           'start.1',
                url:            '/1',
                templateUrl:    'views/onboard/steps/01-problem.ejs',
                screen:         'onboarding',
                stage:          'steps',
                step:           1,
                title:          'The Problem'
            },
            {
                name:           'start.2',
                url:            '/2',
                templateUrl:    'views/onboard/steps/02-urgency.ejs',
                screen:         'onboarding',
                stage:          'steps',
                step:           2,
                title:          'How Critical Is It?'
            },
            {
                name:           'start.3',
                url:            '/3',
                templateUrl:    'views/onboard/steps/03-info.ejs',
                screen:         'onboarding',
                stage:          'steps',
                step:           3,
                title:          'How Can We Contact You?'
            },
            {
                name:           'start.4',
                url:            '/4',
                templateUrl:    'views/onboard/steps/04-plans.ejs',
                screen:         'onboarding',
                stage:          'steps',
                step:           4,
                title:          'Pick a Plan That Works for You'
            },
            {
                name:           'start.error',
                url:            '/error',
                templateUrl:    'views/onboard/steps/error.ejs',
                screen:         'onboarding',
                stage:          'error',
                title:          'Ooops!'
            }
        ];
        var homeState =
        {
            name:           'home',
            url:            '/',
            templateUrl:    'views/home.html',
            controller:     'MainController',
            screen:         'home'
        };
        var accountSetup =
        {
            name:           'setup',
            url:            '/setup',
            templateUrl:    'views/dashboard/setup/setup.html',
            controller:     'DashboardController',
            screen:         'setup'
        };
        var dashboard = 
        {
            name:           'dashboard',
            url:            '/dashboard',
            templateUrl:    'views/dashboard/home.ejs',
            controller:     'DashboardController',
            screen:         'dashboard-home'
        };
        var dashboardScreens = [
            {
                name:       'dashboard.tickets',
                url:        '/tickets',
                templateUrl:'views/dashboard/tickets.ejs',
                screen:     'dashboard-tickets'
            },
            {
                name:       'dashboard.messages',
                url:        '/messages',
                templateUrl:'views/dashboard/messages.html',
                screen:     'dashboard-messages'
            },
            {
                name:       'dashboard.notifications',
                url:        '/notifications',
                templateUrl:'views/dashboard/notifications.html',
                screen:     'dashboard-notifications'
            },
            {
                name:       'dashboard.profile',
                url:        '/profile',
                templateUrl:'views/dashboard/profile.html',
                screen:     'dashboard-profile'
            }
        ];
        $stateProvider.state(homeState);
        $stateProvider.state(completeState);
        $stateProvider.state(accountSetup);
        // $stateProvider.state(dashboard);
        $stateProvider.state(onboarding);
        $stateProvider = setupStates($stateProvider, onboardingScreens);
        // $stateProvider = setupStates($stateProvider, dashboardScreens);
        $stateProvider.state('blog', {
            url: '/blog'
        });
        $stateProvider.state('login', {
            url: '/login'
        });
        $stateProvider.state('logout', {
            url: '/logout'
        });
        $stateProvider.state('setupdash', {
            url: '/setupdash'
        });

        $locationProvider.html5Mode(true);
    }
]);
tsc.run(['$rootScope', '$window', '$state', function($rootScope, $window, $state) {
  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams) {
        if(toState.name === 'blog') {
            $window.open(__env.baseUrl+'/blog', '_self');
        }
        if(toState.name === 'logout') {
            $window.open(__env.baseUrl+'/logout', '_self');
        }
        if(toState.name === 'login') {
            $window.open(__env.baseUrl+'/login', '_self');
        }
        if(toState.name === 'setupdash') {
            $window.open(__env.baseUrl+'/setup', '_self');
        }
    });
}]);
