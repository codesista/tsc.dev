var fs = require('fs');
var contents = fs.readFileSync("./config/default.json");
var options = JSON.parse(contents);
module.exports = {
    init: function(app) {
        Object.keys(options).forEach(function(option) {
            app.set(option, options[option]);
        });
    }
};
