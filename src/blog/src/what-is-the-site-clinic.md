---
title: What is The Site Clinic
date: 2016-10-16
layout: post.ejs
---

What is The Site Clinic? Well, exactly what it sounds like. You have a website for your small business, side project, or your department at the university. It's been working just fine for a while until you realize that it's taking way too long to load a few pages.

Or maybe one day you installed a WordPress plugin or theme and the entire site just up and disappeared.

Or maybe your webhost is sending you messages about some kind of security breach and you have absolutely no idea what they're talking about.

Or maybe you need to fine tune one of the business processes on the homepage.

You're technical enough to get the site up, but you don't have the time to go fiddling with the engines this time arouond. You're juggling deadlines, customers, and bosses and you could really use your own development team.

### Time to call in the professionals
Bringing your site to The Site Clinic is like walking into a clinic or urgent care when something ails you and the over-the-counter stuff won't cut it any longer. Of course we'd love to take care of your needs before it gets to that point, but we understand that you wanted to make sure you did all your research and used all the resources you had beforehand.

Just think of us as another resource you can add to your toolkit. Our technicians are veterans at all manners of web development and repair, from rescuing a WordPress site from plugin purgatory to tweaking web servers to get them running and peak performance.

And we don't just fix websites; we build them too! If you have a new project, feature, or startup you'd like to launch, we'll work with you to get a minium viable project up in three weeks. We like to use agile development methodology which helps us help you in a efficient and personable manner.

## How this works
Working with us is simple. You [pick a plan](https://thesiteclinic.com/plans) &mdash; Bronze, Silver, or Gold &mdash; and start sending us tickets with details of the job you'd like us to complete for you. You'll get assigned a technician on each job who will work with you to resolve your issue as soon as possible.

Because of the nature of the web, we'll have to ask for a lot of sensitive information like passwords and usernames. We have a secured way of doing this to make sure we capture those details as safely as possible.

## Who's behind all this?
My name is Ann Daramola and I'm a 15-year veteran of the best and worst parts of web development. I've worked with startups, small businesses, and educational institutions across the world to develop almost every kind of web development project you can imagine.

## Ready to get started?
[File your first ticket today!](https://thesiteclinic.com/start)
