var Metalsmith  = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');
var rename      = require('metalsmith-rename');
var sitemap     = require('metalsmith-sitemap');
var excerptor   = require('metalsmith-excerptor');
var dated       = require('metalsmith-date-formatter');
var bloglist    = require('./metalsmith-bloglist');
var config      = require('./config/default');

Metalsmith(__dirname)
  .metadata({
    title: "The Site Clinic",
    description: "We got this.",
    url: config.urls.base,
    baseUrl: config.urls.base,
    generator: "ejs"
  })
  .source('./src/blog/src')
  .destination('./public/blog')
  .clean(false)
  .use(markdown())
  .use(dated({
    dates: [
        {
            key: 'date',
            format: 'MMMM Do YYYY, h:mm a'
        }
    ]
  }))
  .use( excerptor({
      maxLength: 500,
      keepImageTag: true,
      ellipsis: '...'
  }))
  .use(permalinks())
  .use(bloglist())
  .use(layouts({
    engine: 'ejs',
    directory: './public/views/blog'
  }))
  .use(sitemap({
    "hostname": config.urls.base
  }))
  .use(rename([
      [/\.html$/, ".ejs"]
  ]))
  .build(function(err, files) {
    if (err) { throw err; }
  });
