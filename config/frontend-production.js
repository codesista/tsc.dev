(function (window) {
  window.__env = window.__env || {};
  window.__env.baseUrl = 'https://thesiteclinic.com';
  window.__env.stripeKey = 'pk_live_gq4NfXc63m7doFkYUpHX1AdI';
  window.__env.stripePlanIds = {
    single: 'single',
    bronze: 'TSCBRONZE201610X079',
    silver: 'TSCSILVER201610X099',
    gold:   'TSCGOLD201610X199'
  };
}(this));