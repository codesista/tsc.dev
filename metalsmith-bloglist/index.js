var fs = require('fs');
var config = require('../config/default');
var baseUrl = config.urls.base+'/';
var sitePath = config.sitePath;
module.exports = plugin;
function byDate(files)
{
    var sortable = [];
    for (var file in files)
          sortable.push([file, files[file]])
    sortable.sort(
        function(a, b) {
            return b[1].date - a[1].date
        }
    )
    return sortable;
}
function writeLinks(listFile,links)
{
    var link;
    for (var i = 0; i < links.length; i++) {
        link = '<article>'+"\n    "+'<h2 class="post-title">'+"\n        "+'<a href="'+links[i].link+'">'+links[i].title+'</a>'
                +"\n    "+'</h2>'+"\n    "+links[i].excerpt+"\n    "+'<p class="read-more">'
                +'<a href="'+links[i].link+'">Continue Reading</a></p>'+"\n"+'</article>'
                +"\n\n"
        fs.appendFile(listFile, link, function(err) {
            if(err) {
                return console.log(err);
            }
        });
    }   
}
function plugin(options){
    var hostname = baseUrl;
    var listFile = sitePath+"/public/pages/blog.list.ejs"
    var links = [];
    fs.unlink(listFile, (err) => {
          if (err) ;
    });
    return function(files, metalsmith, done){
        setImmediate(done);
        var sorted = byDate(files);
        for (var i = 0; i < sorted.length; i++) {
            var data = sorted[i][1];
            var link = hostname+data.path;
            links.push({title: data.title,link:link,excerpt:data.excerpt});
        }
        writeLinks(listFile,links);
    }
}