var gulp    = require ('gulp');
var order   = require('gulp-order');
var concat  = require('gulp-concat');
var uglify  = require('gulp-uglify');
var gutil   = require('gulp-util');
var sass    = require('gulp-sass');

var paths = {
  'npm'       : './node_modules/',
  'frontjs'   : './src/client/js/',
  'frontcss'  : './src/client/scss/',
  'js'        : './public/assets/js/lib/',
  'css'       : './public/assets/css/',
  'conf'      : './config/',
  'confDest'  : './public/assets/js/'
};

gulp.task('styles-lib', function()
  {
    return gulp.src([
      paths.npm+'bootstrap/dist/css/bootstrap.min.css'
    ])
    .pipe(gulp.dest(paths.css));
  }
);

gulp.task('styles-main', function()
  {
    return gulp.src([
      paths.frontcss+"/main/*.scss"
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('tsc.css'))
    .pipe(gulp.dest('./public/assets/css'));
  }
);

gulp.task('styles-pages', function()
  {
    return gulp.src([
      paths.frontcss+"/pages/*.scss"
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css'));
  }
);
gulp.task('config-frontend',function(){
  return gulp.src([
    paths.conf+'frontend.js'
  ])
  .pipe(concat('config.js'))
  .pipe(gulp.dest(paths.confDest))
});
gulp.task('scripts-lib', function(){
  return gulp.src([
    paths.npm+'jquery/dist/jquery.min.js',
    paths.npm+'bootstrap/dist/js/bootstrap.min.js',
    paths.npm+'jquery.backstretch/jquery.backstretch.min.js',
    paths.npm+'waypoints/lib/jquery.waypoints.min.js',
    paths.npm+'angular/angular.js',
    paths.npm+'angular-route/angular-route.js',
    paths.npm+'angular-cookies/angular-cookies.js',
    paths.npm+'angular-ui-router/release/angular-ui-router.js',
    ])
    .pipe(gulp.dest(paths.js));
});

gulp.task('scripts-frontend', function(){
  return gulp.src([
    paths.frontjs+'*.js',
    paths.frontjs+'**/*.js'
    ])
    .pipe(uglify()).on('error', gutil.log)
    .pipe(concat('tsc.js'))
    .pipe(gulp.dest('./public/assets/js'));
});

gulp.task('watch', function(){
  gulp.watch(paths.frontjs+'**/*.js',['scripts-frontend']);
  gulp.watch(paths.frontjs+'*.js',['scripts-frontend']);
  gulp.watch(paths.frontcss+"/main/*.scss",['styles-main']);
  gulp.watch( paths.frontcss+"/pages/*.scss",['styles-pages']);
});

gulp.task('default', 
    [
      'scripts-lib',
      'scripts-frontend',
      'styles-lib',
      'styles-pages',
      'styles-main',
      'config-frontend'
    ]
);
gulp.task('strap',
    [
      'scripts-lib',
      'scripts-frontend',
      'styles-lib',
      'styles-main',
      'styles-pages'
    ]
);